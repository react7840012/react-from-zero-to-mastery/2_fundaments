const TemplateExpressions = () => {

    const name = 'Fernando';
    const data = {
        age: 31,
        job: 'Programmer'
    }

    return (
        <div>
            <h1>Hello {name}, how are you?</h1>
            <p>You work as: { data.job }</p>
            <p>{5 + 5}</p>
            <p>{console.log("Hello there, this is the JSX React")}</p>
        </div>
    )

}

export default TemplateExpressions;