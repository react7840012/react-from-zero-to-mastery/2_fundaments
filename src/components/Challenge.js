const Challenge = () => {

    const sum = () => {
        let value1 = document.getElementById('value1').value
        let value2 = document.getElementById('value2').value
        
        if (value1 === '') {
            alert("Plz, type a number in the first value")
            return false
        }

        if (value2 === '') {
            alert("Plz, type a number in the second value")
            return false
        }

        let total = parseInt(value1) + parseInt(value2);
        console.log("The sum is: ", total)
        alert("The sum is: " + total)
    }

    return (
        <div>
            <div>
                <label htmlFor="valor1">Type the first value</label>
                <input type="number" id="value1"/>
            </div>
            <div>
                <label htmlFor="valor1">Type the second value</label>
                <input type="number" id="value2"/>
            </div>
            <button onClick={sum}>Calcular a soma</button>
        </div>
    );

}

export default Challenge;