import MultiUsedComponent from "./MultiUsedComponent";

const FirstComponent = () => {
    return (
        <div>
            <h1>My First Component</h1>
            <p className="teste">A Paragraph</p>
            <MultiUsedComponent/>
        </div>
    )
}

export default FirstComponent;