const Events = () => {

    const handleMyEvent = (e) => {
        console.log(e)
        console.log("event activated")
    };

    const renderSomething = (x) => {
        if (x) {
            return <h1>Rendering this</h1>;
        } else {
            return <h1>And this too</h1>;
        }
    }

    return (
        <div>
            <div>
                <button onClick={handleMyEvent} >Click Here!</button>
            </div>
            <div>
                <button onClick={() => { console.log("Clicked") }}>Click here too!</button>
                <button onClick={() => {
                    if (true) {
                        console.log("This shouldn't be here")
                    }
                }}>What about here? </button>
            </div>
            {renderSomething(true)}
            {renderSomething(false)}
        </div>
    )
}

export default Events;