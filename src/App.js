// styles
import './App.css';

// components
import FirstComponent from './components/FirstComponent';
import TemplateExpressions from './components/TemplateExpressions';
import MultiUsedComponent from './components/MultiUsedComponent';
import Events from './components/Events';
import Challenge from './components/Challenge';

function App() {
  return (
    <div className="App">
      <h1>Fundamentals of React</h1>
      <FirstComponent/>
      <TemplateExpressions/>
      <MultiUsedComponent/>
      <Events/>
      <Challenge/>
    </div>
  );
}

export default App;
